<!-- Begin MailChimp Signup Form -->

<div class="mc_embed_signup cf">
	<form action="<?php echo $attrs['action'] ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
	    <div id="mc_embed_signup_scroll">
		
		<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="Type Your Email..." required>
	    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="<?php echo $attrs['secure'] ?>" tabindex="-1" value=""></div>
	    <!--<span class="buy-product-btn"></span>--><input type="submit" value="<?php echo isset($attrs['label']) ? $attrs['label'] : 'Join >' ?>" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
	</form>
</div>


<!--End mc_embed_signup-->  